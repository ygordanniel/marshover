package br.com.meta;

import br.com.meta.marsrover.MarsRover;
import br.com.meta.marsrover.space.CardinalPoint;
import br.com.meta.marsrover.space.Coordinates;
import br.com.meta.marsrover.space.Plateau;
import java.util.concurrent.Semaphore;
import org.junit.Assert;
import org.junit.Test;

public class MarsRoverTest {

    @Test
    public void canRunCommandLMLMLMLMM() {
        Plateau plateau = new Plateau(5, 5);
        Coordinates coordinates = new Coordinates(1, 2);
        MarsRover marsRover = new MarsRover("A", coordinates, CardinalPoint.N, plateau, new Semaphore(1));
        marsRover.init("LMLMLMLMM");
        marsRover.run();
        Assert.assertEquals("1 3 N", marsRover.toString());
    }

    @Test
    public void canRunCommandMMRMMRMRRM() {
        Plateau plateau = new Plateau(5, 5);
        Coordinates coordinates = new Coordinates(3, 3);
        MarsRover marsRover = new MarsRover("A", coordinates, CardinalPoint.E, plateau, new Semaphore(1));
        marsRover.init("MMRMMRMRRM");
        marsRover.run();
        Assert.assertEquals("5 1 E", marsRover.toString());
    }

    @Test
    public void canTurnLeft() {
        Plateau plateau = new Plateau(5, 5);
        Coordinates coordinates = new Coordinates(1, 2);
        MarsRover marsRover = new MarsRover("A", coordinates, CardinalPoint.S, plateau, new Semaphore(1));
        marsRover.init("L");
        marsRover.run();
        Assert.assertEquals("1 2 E", marsRover.toString());
    }

    @Test
    public void canTurnRight() {
        Plateau plateau = new Plateau(5, 5);
        Coordinates coordinates = new Coordinates(1, 2);
        MarsRover marsRover = new MarsRover("A", coordinates, CardinalPoint.W, plateau, new Semaphore(1));
        marsRover.init("R");
        marsRover.run();
        Assert.assertEquals("1 2 N", marsRover.toString());
    }

    @Test
    public void cantMove() {
        Plateau plateau = new Plateau(5, 5);
        Coordinates coordinates = new Coordinates(5, 5);
        MarsRover marsRover = new MarsRover("A", coordinates, CardinalPoint.N, plateau, new Semaphore(1));
        marsRover.init("M");
        marsRover.run();
        Assert.assertEquals("5 5 N", marsRover.toString());
    }

    @Test
    public void doNothing() {
        Plateau plateau = new Plateau(5, 5);
        Coordinates coordinates = new Coordinates(5, 5);
        MarsRover marsRover = new MarsRover("A", coordinates, CardinalPoint.N, plateau, new Semaphore(1));
        marsRover.init("Y");
        marsRover.run();
        Assert.assertEquals("5 5 N", marsRover.toString());
    }

}