package br.com.meta.marsrover;

/**
 * Interface provide a initialization of the rover.
 */
public interface Initializable {
    void init(final String initCommand);
}
