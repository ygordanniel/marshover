package br.com.meta.marsrover.command.impl;

import br.com.meta.marsrover.MarsRover;
import br.com.meta.marsrover.command.ICommand;

/**
 * Command to turn the rover to the right.
 * See {@link ICommand}
 */
public class TurnRightCommandImpl implements ICommand {

    @Override
    public void execute(final MarsRover rover) {
        rover.turnRight();
    }
}
