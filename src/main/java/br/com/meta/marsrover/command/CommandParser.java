package br.com.meta.marsrover.command;

import br.com.meta.marsrover.command.impl.MoveCommandImpl;
import br.com.meta.marsrover.command.impl.TurnLeftCommandImpl;
import br.com.meta.marsrover.command.impl.TurnRightCommandImpl;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Parser to parser a series of commands to the strategy format.
 * See {@link ICommand}
 */
public final class CommandParser {

    private static final String SPLIT_INDEX = "";
    private static final String L = "L";
    private static final String R = "R";
    private static final String M = "M";
    private static final List<String> ALLOWED_COMMANDS = Arrays.asList(L, R, M);

    private CommandParser() {}

    /**
     * Parse a series of commands in {@link String} format to {@link ICommand} format.
     * @param commandsString Commands to be parsed.
     * @return List of commands in {@link ICommand}
     */
    public static List<ICommand> fromString(final String commandsString) {
        return Arrays.stream(commandsString.split(SPLIT_INDEX))
                .filter(ALLOWED_COMMANDS::contains)
                .map(command -> {
                    switch (command) {
                        case L: return new TurnLeftCommandImpl();
                        case R: return new TurnRightCommandImpl();
                        case M: return new MoveCommandImpl();
                        default: return null;
                    }
                }).collect(Collectors.toList());
    }

}
