package br.com.meta.marsrover.command.impl;

import br.com.meta.marsrover.MarsRover;
import br.com.meta.marsrover.command.ICommand;

/**
 * Command to turn the hover to the left.
 * See {@link ICommand}
 */
public class TurnLeftCommandImpl implements ICommand {

    @Override
    public void execute(final MarsRover rover) {
        rover.turnLeft();
    }
}
