package br.com.meta.marsrover.command;

import br.com.meta.marsrover.MarsRover;

/**
 * Strategy for a better abstraction of commands the rover will run.
 */
public interface ICommand {

    void execute(final MarsRover rover);

}
