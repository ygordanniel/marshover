package br.com.meta.marsrover.command.impl;

import br.com.meta.marsrover.MarsRover;
import br.com.meta.marsrover.command.ICommand;

/**
 * Command to move the rover.
 * See {@link ICommand}
 */
public class MoveCommandImpl implements ICommand {

    @Override
    public void execute(final MarsRover rover) {
        rover.move();
    }
}
