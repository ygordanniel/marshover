package br.com.meta.marsrover;

import br.com.meta.marsrover.space.CardinalPoint;
import br.com.meta.marsrover.space.Coordinates;
import br.com.meta.marsrover.space.Plateau;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Huston {

  public static void main(String[] args) {
      Semaphore semaphore = new Semaphore(1);
      List<Thread> rovers = new ArrayList<>();

      Plateau plateau = new Plateau(5, 5);

      Coordinates coordinates = new Coordinates(1, 2);
      MarsRover marsRoverA = new MarsRover("A", coordinates, CardinalPoint.N, plateau, semaphore);
      marsRoverA.init("LMLMLMLMM");

      rovers.add(marsRoverA);

      coordinates = new Coordinates(3, 3);
      MarsRover marsRoverB = new MarsRover("B", coordinates, CardinalPoint.E, plateau, semaphore);
      marsRoverB.init("MMRMMRMRRM");

      rovers.add(marsRoverB);

      rovers.forEach(Thread::start);
  }
}
