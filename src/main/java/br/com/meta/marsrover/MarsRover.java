package br.com.meta.marsrover;

import br.com.meta.marsrover.command.CommandParser;
import br.com.meta.marsrover.command.ICommand;
import br.com.meta.marsrover.space.CardinalPoint;
import br.com.meta.marsrover.space.Coordinates;
import br.com.meta.marsrover.space.Plateau;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * Class representing a Rover on Mars.
 */
public class MarsRover extends Thread implements Initializable {

    // Rover name
    private String name;
    // Rover location on Mars plateau
    private Coordinates coordinates;
    // Rover cardinal point orientation
    private CardinalPoint orientation;
    // Mars plateau
    private Plateau plateau;
    // Semaphore for thread safe operation
    private Semaphore semaphore;
    // List of commands the rover will execute
    private List<ICommand> commands;

    public MarsRover(String name, Coordinates coordinates, CardinalPoint orientation,
            Plateau plateau, Semaphore semaphore) {
        this.name = name;
        this.coordinates = coordinates;
        this.orientation = orientation;
        this.plateau = plateau;
        this.semaphore = semaphore;
    }

    /**
     * Moves rover to new {@link CardinalPoint} position if it is within the plateau.
     */
    public void move() {
        Coordinates newPosition = this.coordinates.move(orientation);
        if(this.plateau.isWithinBounds(newPosition)) {
            this.coordinates = newPosition;
        }
    }

    /**
     * Turns rover 90º left
     */
    public void turnLeft() {
        this.orientation = this.orientation.left();
    }

    /**
     * Turns rover 90º right
     */
    public void turnRight() {
        this.orientation = this.orientation.right();
    }

    @Override
    public void init(String initCommand) {
        this.commands = CommandParser.fromString(initCommand);
    }

    @Override
    public void run() {
        try {
            semaphore.acquire();
            System.out.println("Running rover " + this.name);
            this.commands.forEach(command -> command.execute(this));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally{
            System.out.println("Rover " + this.name + " stopped at " + toString());
            semaphore.release();
        }
    }

    @Override
    public String toString() {
        return coordinates.toString() + " " + orientation.toString();
    }

}
