package br.com.meta.marsrover.space;

/**
 * This class represents a location on a cartesian plane.
 */
public class Coordinates {
    private int x;
    private int y;

    public Coordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Coordinates() {
        this.x = 0;
        this.y = 0;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    /**
     * Move the location based on a {@link CardinalPoint}
      * @param orientation cardinal point that the move will be based
     * @return new location after the movement
     */
    public Coordinates move(CardinalPoint orientation) {
        return new Coordinates(this.x + orientation.getMoveSizeOnX(), this.y + orientation.getMoveSizeOnY());
    }

    /**
     * Checks if a given {@link Coordinates} is lower or equal than this
     * @param coordinates {@link CardinalPoint} to check
     * @return true if is equal or lower, false otherwise
     */
    public boolean isLowerOrEqual(Coordinates coordinates) {
        return this.x >= coordinates.getX() && this.y >= coordinates.getY();
    }

    /**
     * Checks if a given {@link CardinalPoint} is higher or equal than this
     * @param coordinates {@link CardinalPoint} to check
     * @return true if is higher or equal, false otherwise
     */
    public boolean isHigherOrEqual(Coordinates coordinates) {
        return this.x <= coordinates.getX() && this.y <= coordinates.getY();
    }

    @Override
    public String toString() {
        return x + " " + y;
    }
}
