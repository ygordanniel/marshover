package br.com.meta.marsrover.space;

/**
 * Class representing Mars surface.
 * This class abstracts a cartesian plane.
 */
public class Plateau {
    // Origin position of the plane
    private Coordinates topRight;
    // End position of the plane
    private Coordinates bottomLeft;

    public Plateau(final int topRightX, final int topRightY) {
        this.topRight = new Coordinates(topRightX, topRightY);
        this.bottomLeft = new Coordinates();
    }

    /**
     * Checks if a given {@link CardinalPoint} is with the cartesian plane.
     * @param coordinates {@link CardinalPoint} to check
     * @return true if the {@link CardinalPoint} is within the cartesian plane, false otherwise
     */
    public boolean isWithinBounds(Coordinates coordinates) {
        return topRight.isLowerOrEqual(coordinates) && this.bottomLeft.isHigherOrEqual(coordinates);
    }
}
