package br.com.meta.marsrover.space;

/**
 * Enum representing the 4 basic Cardinal Points.
 */
public enum CardinalPoint {
    N(0, 1) {
        @Override
        public CardinalPoint right() {
            return E;
        }

        @Override
        public CardinalPoint left() {
            return W;
        }
    },
    S(0, -1) {
        @Override
        public CardinalPoint right() {
            return W;
        }

        @Override
        public CardinalPoint left() {
            return E;
        }
    },
    E(1, 0) {
        @Override
        public CardinalPoint right() {
            return S;
        }

        @Override
        public CardinalPoint left() {
            return N;
        }
    },
    W(-1, 0) {
        @Override
        public CardinalPoint right() {
            return N;
        }

        @Override
        public CardinalPoint left() {
            return S;
        }
    };

    /**
     * Size of movement in X axis. This abstraction was necessary for a better control of the rover
     * movement.
     */
    private final int moveSizeOnX;
    /**
     * Size of movement in Y axis. This abstraction was necessary for a better control of the rover
     * movement.
     */
    private final int moveSizeOnY;

    CardinalPoint(final int moveSizeOnX, final int moveSizeOnY) {
        this.moveSizeOnX = moveSizeOnX;
        this.moveSizeOnY = moveSizeOnY;
    }

    /**
     * Get the new {@link CardinalPoint} when moving to right;
     * @return new {@link CardinalPoint}
     */
    public abstract CardinalPoint right();
    /**
     * Get the new {@link CardinalPoint} when moving to left;
     * @return new {@link CardinalPoint}
     */
    public abstract CardinalPoint left();

    public int getMoveSizeOnX() {
        return moveSizeOnX;
    }

    public int getMoveSizeOnY() {
        return moveSizeOnY;
    }
}
